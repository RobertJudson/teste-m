# Introdução

Projeto de criação de uma lista de clientes com CPT e ACF.

__Acesso ao painel administrativo__

__Usuário:__ robert<br>
__Senha:__ robert

__Acesso ao banco de dados__

__Nome do banco:__ `teste-m`<br>
__Usuário:__ `root`<br>
__Senha:__ ` `<br>
__Host:__ `localhost`<br>

## Instalando o JSON Server

Para instalar JSON Server você pode utilizar o NPM ou o Yarn

```
npm install -g json-server
```

Agora é necessário criar um arquivo json, como vamos utilizar um modelo predefinido, o arquivo `simple.json` servirá como modelo e está presente no projeto dentro da pasta `json-server`.

Iniciando o JSON Server utilizando o arquivo predefinido

```bash
json-server --watch sample.json
```

Agora você pode acessar [http://localhost:3000/](http://localhost:3000/), seu servidor já vai estar funcionando.

Para mais detalhes e documentação acesse o repositório oficial: [https://github.com/typicode/json-server](https://github.com/typicode/json-server)

## Instalando o Wordpress

Para instalar o wordpress você pode utilizar algum servidor Apache a sua escolha, nesse caso foi utilizado o `Xampp` pois ele já vem incluso um banco de dados e de fácil utilização.

Download do Xampp: [https://www.apachefriends.org/download.html](https://www.apachefriends.org/download.html)

Com o Xampp instalado basta executar-lo e iniciar os módulos `Apache` e `MySQL` presentes nele.

Agora é necessário mover todo o projeto para a pasta do Xampp `C:/xampp/htdocs/`, __não é necessário mover a pasta json-server__.

## Importanto o banco de dados

Acessando o banco de dados através do phpmyadmin: [http://localhost/phpmyadmin](http://localhost/phpmyadmin)

Agora é necessário criar um novo banco chamado `teste-m`, ir na aba `importar`, escolher o arquivo `teste-m.sql`, presente na pasta `database`, e executar a importação.

Pronto, com todos os passos feitos agora basta acessar [http://localhost/teste-m](http://localhost/teste-m)

![Todos os clientes](https://gitlab.com/RobertJudson/teste-m/-/raw/06653956fc8a1eeb7ac69874f1cdc686ec5457f2/wp-content/uploads/2021/06/clientes.PNG)
![Todos os clientes](https://gitlab.com/RobertJudson/teste-m/-/raw/06653956fc8a1eeb7ac69874f1cdc686ec5457f2/wp-content/uploads/2021/06/cliente.PNG)
