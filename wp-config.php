<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'teste-m' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S[+sn4K+!&zYK]$f/zxoX7nl+pOon_=!tk,5CfiLtD{]8p^#;!>C1bZ$HVj39j7|' );
define( 'SECURE_AUTH_KEY',  'LYy.Rk-heP=Z}=O9uT/$G3 MMH:0Gmi?huA1W~w-Pe7vV&Un)67*2Q|9}C>P1[b+' );
define( 'LOGGED_IN_KEY',    'zw8JBwK>)vw_3Wjw^^k96N$uwAdyQ1I]=i/=!k$]g|uoxE``/LO;mD$O196m]l3;' );
define( 'NONCE_KEY',        '&4kpk=9U1ucs5IzqA&{QqY`m+CUs%(iFHF%BNjfDC>r2D>H6q1TGG>.@VIEyTSJI' );
define( 'AUTH_SALT',        ' (bb-smE%!8*>*r}E(3M7ph$aehDRqvqUFF=d^RWPegX{7hVo?UL4f^_p:i2qGIP' );
define( 'SECURE_AUTH_SALT', '<h6NRA@r8P5x19kr~AQU+mI>sL>6N_|H[ >Ln?i(Qt!zq1S,B%:{4$[|Dh?;*S^i' );
define( 'LOGGED_IN_SALT',   '>Q@;YD4LjUPX-?1>~W;rZi3e=O@SuayH1G4?l:c3oe]$8=0tbEy6OI{(/L`qJ9-W' );
define( 'NONCE_SALT',       '89bn91f?pTt;bA#U!y_JGjQ=mUR<3.%(ArqN)A-q:}B+JWoKv# gLr5qQVQmhhkW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
