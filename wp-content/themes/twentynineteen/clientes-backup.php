<?php
	/*
		Template name: Clientes
 	*/
	the_content(); 
?>
<!-- CSS-START -->
<style>
  body.customize-support{
	background: linear-gradient(#6d16cf, #c81f90);
	display: flex;
	align-items: center;
	justify-content: center;
	font-family: roboto;
  }
  section{
	width: 100%;
	max-width: 600px;
	min-width: 360px;
	display: flex;
	flex-direction: column;
	align-items: center;
	margin-bottom: 60px;
  }
  section h2{
	color: #fefefe;
	font-size: 50px;
	font-weight: 600;
  }
  section h2:before{
	display: none;
  }
  div.list-area{
	background: rgba(0, 0, 0, .5);
	padding: 32px 64px;
	border-radius: 10px;
	box-shadow: 0 0 16px rgba(0, 0, 0, .4);
	width: 100%;
	max-width: 500px;
	min-width: 360px;
	display: flex;
	justify-content: center;
  }
  .customize-support section ul{
  	display: flex;
	flex-wrap: wrap;
	width: 100%;
	padding: 0;
  }
  .customize-support section ul li{
  	list-style: none;
	border-radius: 12px;
	border: 1px solid rgba(255, 255, 255, .1);
	width: 100%;
	overflow: hidden;
	margin-bottom: 8px;
  }
  .customize-support section ul li:hover{
  	background: rgba(0, 0, 0, .3);
  }
  .customize-support section ul li a{
	display: flex;
	justify-content: start;
	align-items: center;
	color: #efefef;
	font-weight: 500;
	font-size: 18px;
	padding: 0 24px;
  }
  .customize-support section ul li img{
	border-radius: 100%;
	margin-right: 8px;
  }
</style>
<!-- CSS-END -->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head(); ?>
	</head>
	<body>

	<section>
		<h2>Lista de Clientes</h2>
		<div class="list-area">

			<ul>
				<?php
					$homePageClients = new WP_Query(array(
						'posts_per_page' => 2,
						'post_type' => 'clientes'
					));

					while($homePageClients->have_posts()){
						$homePageClients->the_post();
						$picture = get_field( "field_60c8eb9ef1e4b" );
						$company = get_field( "field_60c8ec05f1e4e" );
						$link =  get_post_permalink(); ?>

						<a href="<?php echo $link ?>">
							<li><img style="height:40px; width:40px; object-fit:cover;" src="<?php echo $picture ?>" alt="foto"><p><?php echo $company ?></p>
						</a>
					<?php }
				?>
			</ul>
		
		</div>
	</section>

	<?php 
		wp_footer();
	 ?>
	</body>
</html>
