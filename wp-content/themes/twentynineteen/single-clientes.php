<?php

the_content();

?>
<!-- CSS-START -->
<style>
  body.customize-support{
	background: linear-gradient(#6d16cf, #c81f90);
	display: flex;
	align-items: center;
	justify-content: center;
	font-family: 'roboto', arial;
  }
  section{
	width: 100%;
	max-width: 500px;
	min-width: 360px;
	display: flex;
	flex-direction: column;
	align-items: center;
	margin-bottom: 60px;
  }
  section h2{
	color: #fefefe;
	font-size: 50px;
	font-weight: 600;
  }
  section h2:before{
	display: none;
  }
  div.list-area{
	background: rgba(0, 0, 0, .5);
	padding: 32px 64px;
	border-radius: 10px;
	box-shadow: 0 0 16px rgba(0, 0, 0, .4);
	width: 100%;
	max-width: 500px;
	min-width: 360px;
	display: flex;
	justify-content: center;
	position: relative;
  }
  div.list-area .back{
	  background: rgba(255, 255, 255, .1);
	  position: absolute;
	  top: 16px;
	  left: 16px;
	  padding: 16px;
	  line-height: 14px;
	  font-size: 18px;
	  font-weight: 800;
	  color: rgba(255, 255, 255, .5);
	  border-radius: 32px;
	  text-decoration: none;
  }
  div.list-area .back:hover{
	  background: rgba(255, 255, 255, .2);
  }
  .customize-support section ul{
  	display: flex;
	flex-wrap: wrap;
	width: 100%;
	padding: 0;
  }
  .customize-support section ul li{
  	list-style: none;
	border-bottom: 1px solid rgba(255, 255, 255, .1);
	width: 100%;
	overflow: hidden;
	margin-bottom: 8px;
	display: flex;
	flex-direction: column;
	color: #efefef;
  }
  .customize-support section ul li:first-child{
	border-bottom: none;
	margin-bottom: 24px;
	justify-content: center;
	align-items: center;
  }
  .customize-support section ul li img{
	border-radius: 100%;
	width: 200px;
	height: 200px;
	object-fit: cover;
  }
  .customize-support section ul li span{
	  color: rgba(255, 255, 255, .6);
	  font-size: 14px;
  }
  .customize-support section ul li p{
	padding: 0;
	margin: 0 0 4px;
	font-size: 16px;
  }
</style>
<!-- CSS-END -->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head(); ?>
	</head>
	<body>

	<section>
		<h2>Cliente</h2>
		<div class="list-area">
		<a class="back" href="javascript:history.back()"><</a>

		<?php 
			$id = get_field( "_id" );
			$index = get_field( "index" );
			$guid = get_field( "guid" );
			$isActive = get_field( "isActive" ) ? 'sim' : 'não';
			$balance = get_field( "balance" );
			$picture = get_field( "picture" );
			$age = get_field( "age" );
			$eyeColor = get_field( "eyeColor" );
			$company = get_field( "company" );
		?>
		<ul>
			<li><img src="<?php echo $picture; ?>" alt="foto"></li>
			<li><span>Empresa:</span><p><?php echo $company; ?></p></li>
			<li><span>Idade:</span><p><?php echo $age; ?> anos</p></li>
			<li><span>Saldo:</span><p>R$ <?php echo $balance; ?></p></li>
			<li><span>Cor dos Olhos:</span><p><?php echo $eyeColor; ?></p></li>
			<li><span>Ativo:</span><p><?php echo $isActive; ?></p>
			</li>
			<li><span>ID:</span><p><?php echo $id; ?></p></li>
			<li><span>Índice:</span><p><?php echo $index; ?></p></li>
			<li><span>Guid:</span><p><?php echo $guid; ?></p></li>
		</ul>
		
		</div>
	</section>

	<?php 
		wp_footer();
	 ?>
	</body>
</html>
